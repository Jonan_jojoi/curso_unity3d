﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public GameObject[] enemigos;
/*
    public GameObject prefabLata;
    public GameObject prefabLata_2;
    public GameObject prefabSujetaLata;*/
    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;

    //private int puntosAnt; // Puntos del frame anterior

    // Start is called before the first frame update
    void Start(  )
    {
        // Aprox. mitad probabilidades
        this.InstanciarEnemigo();
    }
    // Update is called once per frame
    void Update()
    {
        textoVidas.GetComponent<Text>().text = "Vidas: " + this.vidas;
        textoPuntos.GetComponent<Text>().text = "Puntos: " + this.puntos;
    }
    // Esto es un nuevo método 
    // (acción, conjunto de instrucciones, función, procedimiento, mensaje...)
    public void CuandoCapturamosEnemigo()
    {
        // Estamos asignando un nuevo valor a la puntuación,
        // que es los puntos actuales + 10 ptos.
        this.puntos = this.puntos + 10;     // this.puntos  += 10;
        this.InstanciarEnemigo();
    }
    public void CuandoPerdemosEnemigo()
    {
        this.vidas = this.vidas - 1;        // this.vidas -= 1;   this.vidas--;
        this.InstanciarEnemigo();
    }
    public void InstanciarEnemigo()
    {
        int numEnemigo = Random.Range(0, enemigos.Length);
        
        GameObject.Instantiate(enemigos[numEnemigo]);
    }
}

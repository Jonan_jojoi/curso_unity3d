﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LataR : MonoBehaviour
{
    public float velocidad;
    GameObject jugador;
    // Start is called before the first frame update
    void Start()
    {
        float posInicioX = Random.Range(-10, 10);
        this.transform.position = new Vector3(posInicioX, 10, 1);
        jugador = GameObject.Find("Jugador-Caballito");

    }
    // Update is called once per frame
    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -0.5f, 0) * Time.deltaTime;

        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position + movAbajo;

        if (this.GetComponent<Transform>().position.y < -0)
        {
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, 0, 0);

          
            if (this.transform.position.x >= jugador.transform.position.x - 3.2f / 2
                && this.transform.position.x <= jugador.transform.position.x + 3.2f / 2)

            {
                GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().CuandoCapturamosEnemigo();
                Destroy(this.gameObject);
            }
            else
            {
                GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().CuandoPerdemosEnemigo();
                Destroy(this.gameObject);
            }
        }

    }
}

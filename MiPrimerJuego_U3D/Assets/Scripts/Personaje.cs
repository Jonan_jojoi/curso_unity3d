﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    // float es un numero decimal
    public float velocidad = 40; // 40 es el valor por defecto
    // Update is called once per frame
    void Update()
    {
      

       if (Input.GetKey(KeyCode.LeftArrow))
        {
            Vector3 vectorMovL = velocidad * Vector3.left * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMovL);
            if (this.GetComponent<Transform>().position.x < -10 ) 
            {
                this.GetComponent<Transform>().position = new Vector3(-10, 0, 0);
                Debug.Log("Choque a la izquierda: ");
            }
        }

       if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 vectorMovR = velocidad * Vector3.right * Time.deltaTime;

            this.GetComponent<Transform>().Translate(vectorMovR);

            if (this.GetComponent<Transform>().position.x > 10 )
            {
                this.GetComponent<Transform>().position = new Vector3(10, 0, 0);
                Debug.Log("Choque a la derecha: ");

            }
        }
    }
}
